package br.com.zyon.loja.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.zyon.loja.componet.FileSaver;
import br.com.zyon.loja.dao.ProductDao;
import br.com.zyon.loja.enums.BookType;
import br.com.zyon.loja.models.Product;

/**
 * @author zyon
 * 
 */
@Controller
@Transactional
@RequestMapping("/produtos")
public class ProductsController {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private FileSaver fileSarver;
	// validacoes basicas o proprio Bean validadtion faz nao sendo necessario a class custumizada
	//pois a validacao fica na propria model
	// @InitBinder
	// protected void initBinder(WebDataBinder binder){
	// binder.setValidator(new ProductValidator());
	// }
	/**
	 * Metodo é chamando quando nao é passado nenhum outro parametro depois do /produtos
	 * @return {@link ModelAndView}
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView list() {
		System.out.println("lista!");
		ModelAndView modelAndView = new ModelAndView("products/list");
		modelAndView.addObject("products", productDao.list());
		return modelAndView;
	}


	/**
	 * @param product
	 * @param bindingResult
	 * @param redirectAttributes
	 * @return String metodo para salvar o produto
	 */
	@RequestMapping(method = RequestMethod.POST, name = "saveProduct")
	public ModelAndView save(MultipartFile summary,@Valid Product product, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return form(product);
		}
		System.out.println(summary.getName() + summary.getOriginalFilename());
		String webPath = fileSarver.write("upload-images", summary);
		product.setSummaryPath(webPath);
		productDao.save(product);
		redirectAttributes.addFlashAttribute("sucesso", "Livro cadastrado com sucesso");
		return new ModelAndView("redirect:produtos");
	}
	/**
	 * @return String metodo para direcionar o usuário para pagina
	 */
	@RequestMapping("/form")
	public ModelAndView form(Product product) {
		ModelAndView modelAndView = new ModelAndView("products/form");
		modelAndView.addObject("types", BookType.values());
		return modelAndView;
	}
}
