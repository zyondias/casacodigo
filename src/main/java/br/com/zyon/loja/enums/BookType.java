package br.com.zyon.loja.enums;

/**
 * @author zyon
 *
 */
public enum BookType {
	/**
	 * Livro digital
	 */
	EBOOK,
	/**
	 * Impresso 
	 */
	PRINTED,
	/**
	 * Ebook mais o impresso 
	 */
	COMBO
}
