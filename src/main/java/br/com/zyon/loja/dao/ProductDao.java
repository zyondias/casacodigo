package br.com.zyon.loja.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.zyon.loja.models.Product;

/**
 * @author zyon
 *
 */
@Repository
public class ProductDao {

	@PersistenceContext
	private EntityManager manager;

	/**
	 * @param product metodo para persistir no banco o produto
	 */
	public void save(Product product) {
		manager.persist(product);
	}

	/**
	 * @return List {@link Product}
	 */
	public List<Product> list() {
		return manager.createQuery("select distinct(p) from Product p join fetch p.prices", Product.class).getResultList();
	}

}
