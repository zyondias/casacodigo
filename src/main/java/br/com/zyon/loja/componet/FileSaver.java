package br.com.zyon.loja.componet;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
/**
 * @author Zyon Dias
 * Class responsavel por salver arquivos
 * */
@Component
public class FileSaver {

	@Autowired
	private HttpServletRequest request;
	
	//Metodo que salva o o arquivo
	public String write(String baseFolder, MultipartFile file){
		//pegando diretorio real 
		String realPath = request.getServletContext().getRealPath("/"+baseFolder);
		try {
			//criando path para ser salvo
			String path = realPath+"/"+file.getOriginalFilename();
			//salvando arquivo
			file.transferTo(new File(path));
			//retornando caminho da imagem
			return baseFolder+ "/"+ file.getOriginalFilename();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
