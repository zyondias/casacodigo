package br.com.zyon.loja.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.zyon.loja.enums.BookType;

/**
 * @author zyon
 *
 */
@Embeddable
public class Price {

	@Column(scale = 2)
	private BigDecimal value;
	private BookType bookType;
	
	/**
	 * @return {@link BigDecimal}
	 */
	public BigDecimal getValue() {
		return value;
	}
	/**
	 * @param value
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	/**
	 * @return {@link BookType}
	 */
	public BookType getBookType() {
		return bookType;
	}
	/**
	 * @param bookType
	 */
	public void setBookType(BookType bookType) {
		this.bookType = bookType;
	}
}
